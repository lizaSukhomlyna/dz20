import java.util.Date;

public class Product {
    private int id;
    private String type;
    private double price;
    private boolean sale = false;
    private Date date;

    public Product(int id, String type, double price) {
        this.id = id;
        this.type = type;
        this.price = price;
        this.date = new Date();
    }

    public Product(int id, String type, double price, boolean sale) {
        this.id = id;
        this.type = type;
        this.price = price;
        this.sale = sale;
        this.date = new Date();
    }

    public String getType() {
        return type;
    }

    public double getPrice() {
        return price;
    }

    public boolean isSale() {
        return sale;
    }

    public Date getDate() {
        return date;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Product{" +
                "type='" + type + '\'' +
                ", price=" + price +
                ", sale=" + sale +
                ", date=" + date +
                '}';
    }

}
