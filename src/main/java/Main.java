import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) throws Throwable {

        ProductsManagerTasks tasks=new ProductsManagerTasks();
        //#Task1
        System.out.println("Task#1");
        System.out.println(tasks.sortProductsByTypeAndPrice("Book",250).toString());

        //Task#2
        System.out.println("Task#2");
        System.out.println(tasks.applySale(10).toString());
        //Task#3
        System.out.println("Task#3");
        System.out.println(tasks.cheapestProduct("Book").toString());

        //Task#4
        System.out.println("Task#4");
        System.out.println(tasks.getTreeLastProducts().toString());
        //Task#5
        System.out.println("Task5");
        System.out.println(tasks.calcFilteredProducts("Book",75));

        //Task#6
        System.out.println("Task6");
        System.out.println(tasks.getProductsGroupedByType().toString());

    }
}

