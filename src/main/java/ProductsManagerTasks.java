import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class ProductsManagerTasks {
    private List<Product> listOfProducts = new ArrayList<>();
    private List<Product> secListOfProducts = new ArrayList<>();

    public ProductsManagerTasks() {
        fillProductsLists();
    }

    private void fillProductsLists() {
        this.listOfProducts.add(new Product(1, "Book", 120));
        this.listOfProducts.add(new Product(2, "Book", 600));
        this.listOfProducts.add(new Product(3, "Book", 610));
        this.listOfProducts.add(new Product(4, "NoBook", 600));
        this.listOfProducts.add(new Product(5, "NoBook", 80));
        this.secListOfProducts.add(new Product(1, "Book", 120, true));
        this.secListOfProducts.add(new Product(2, "Book", 600, true));
        this.secListOfProducts.add(new Product(3, "Book", 610));
        this.secListOfProducts.add(new Product(4, "NoBook", 600));
        this.secListOfProducts.add(new Product(5, "NoBook", 80, true));
        this.secListOfProducts.add(new Product(6, "Book", 23, true));
        this.secListOfProducts.add(new Product(7, "Book", 24, true));
    }

    public List<Product> sortProductsByTypeAndPrice(String type, double price) {
        return listOfProducts.stream()
                .filter(product -> product.getType().equals(type) && product.getPrice() > price)
                .collect(Collectors.toList());
    }

    public List<Product> applySale(int sale) {
        return secListOfProducts.stream()
                .filter(product -> product.isSale() && product.getType().equals("Book"))
                .peek(product -> product.setPrice(product.getPrice() / sale))
                .collect(Collectors.toList());
    }

    public Product cheapestProduct(String type) throws Throwable {
        Optional<Product> min = secListOfProducts.stream()
                .filter(product -> product.getType().equals(type))
                .min(Comparator.comparingDouble(Product::getPrice));
        Product minPr = null;
        if (min.isPresent()) {
            minPr = min.get();
        }
        min.orElseThrow(() -> new Throwable("Продукт категории Book не найден"));
        return minPr;
    }

    public List<Product> getTreeLastProducts() {
        return secListOfProducts.stream()
                .sorted((o1, o2) -> (int) (o2.getDate().getTime() - o1.getDate().getTime()))
                .limit(3)
                .collect(Collectors.toList());

    }

    public double calcFilteredProducts(String type, double price) {
        String yearToday = new SimpleDateFormat("Y").format(new Date());
        return secListOfProducts.stream()
                .filter(product -> product.getType().equals(type) && product.getPrice() <= price &&
                        new SimpleDateFormat("Y").format(product.getDate()).equals(yearToday))
                .mapToDouble(Product::getPrice).sum();
    }

    public Map<String, List<Product>> getProductsGroupedByType() {
        return secListOfProducts.stream()
                .collect(Collectors.groupingBy(Product::getType));
    }
}
